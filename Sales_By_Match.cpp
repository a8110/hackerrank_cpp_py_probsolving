//           ۝۞ بسم الله الرحمن الرحيم  ۞۝
#include "iostream"
#include <vector>
#include "bits/stdc++.h"
#include <map>

using namespace std;

int getCount(vector<int> array_);

int main(){

    cout << "Glory be to Allah...";
    cout << "\n";
    /**

       ||--- ---     _____     ~   ||----
       ||   |  ||   ||   ||    ||  ||   ||
       ||   |  ||   ||___||_   ||  ||   ||
    */


    /*                    ___
        ||\/||   ___    ||___|
        ||  ||  |___|_  ||

    */

    /*map<string, double> products;

    products["R9 280X"] = 1950.75;
    products["R9 270X"] = 1630.5;
    products["GTX 1650"] = 3000;

    map <string, double> :: iterator iter;


    for(iter = products.begin(); iter != products.end(); iter++){
        cout << (*iter).first << "  " << (*iter).second << "\n";
    }
    */
    vector<int> array_ = {1, 1, 3, 1, 2, 1, 3, 3, 3, 3};

    cout << getCount(array_);



    return 0;
}


/**
    ___
  ||   ||  ||   ||     ||     ||     ___    ||----
  ||   ||  ||   ||   --||-- --||--  |___|   ||
  ||___||  ||___||__   ||__   ||__  |____   ||

*/
int getCount(vector<int> array_){
    int size_ = array_.size();

    //For sorting an array...
    sort(array_.begin(), array_.end());



    //Initialise Map from list elements...
    map<int, int> values_map;
    for (int i=0; i < size_; i++){
        values_map[array_[i]] = 0;
    }



    //Print out Map values; _USING <iterator>...
    map<int, int> :: iterator iter;


    int count_ = 0;

    for (int i = 0; i < size_; i++){
        for (iter = values_map.begin(); iter != values_map.end(); iter++){
           if ((*iter).first == array_[i]){
                (*iter).second += 1;

                if ((*iter).second % 2 == 0){
                    count_ += 1;
                }
           }

        }

    }

    return count_;


}
