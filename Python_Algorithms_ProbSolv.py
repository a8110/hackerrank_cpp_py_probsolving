        #  ۝۞ بسم الله الرحمن الرحيم  ۞۝                                    

'''
               |
   |▌▌▌▌▌▌▌▌▌ ▌▌▌▌▌▌▌▌▌▌   ▌▌    ▌▌▬▬▬▬▬▬▬  ▌▌   ▌▌
   |          ▌▌      ▌▌   ▌▌    ▌▌         ▌▌___▌▌
   |▌▌▌▌▌▌▌▌▌ ▌▌      ▌▌ ▬▬▬▬▬▬  ▌▌              ▌▌
   |          ▌▌      ▌▌   ▌▌    ▌▌              ▌▌
   |▌▌▌▌▌▌▌▌▌ ▌▌      ▌▌   ▌▌___ ▌▌          ____▌▌

'''

# Problem_1 <grading students>.
def gradingStudents(grades):
    d = int(input())
    for b in range(d):
        f = float(input())
        r = 3
        for i in range(40,105,5) :
            if f <=i:
                if i - f < 3 :
                    f = i
                    break
    print(int(f))


#Problem_2 <count oranges & apples>.
def countAppOrng(s, t, a, b, apples, oranges):
    l = list(map(int,input().split())) # Area of the house.
    r = list(map(int,input().split()))# position of trees (a,b)
    k = list(map(int,input().split()))#apples & oranges fall from trees.
    j = list(map(int,input().split())) # distance from apples and house.
    h = list(map(int,input().split())) # distance from oranges & house.
    countA = 0 #for apples
    for i in j:
        i+=r[0]
        if i >= l[0] and i <= l[1]:    
            countA+=1
        else:
            pass
    print(countA)
    countB = 0 #for oranges
    for m in h:
        m+=r[1]
        if m >= l[0] and m <= l[1]:
            countB+=1
        else:
            pass
    print(countB)


#Problem_3 <Number of linear jumps>
def kangaroo(x1, v1, x2, v2):
    jump = True
    i = 1
    for n in range(10000):
        if x1+(v1*i) == x2+(v2*i):
            print("YES")
            break
            #jump = False
        i+=1
    notJump = True
    y = 1
    for t in range(10000):
        if x1+(v1*i) != x2+(v2*i):
            print("NO")
            break
            #notJump = False
        y+=1


#Problem_4 <Between Two Sets>
def getTotalX(a, b):
    # Write your code here
    val = 0

    for i in range(max(a), min(b) + 1):
        
        if sum([i % ext_val == 0 for ext_val in a]) == len(a) :
            
            if sum([ext_val % i == 0 for ext_val in b]) == len(b):

                val+=1
    return val


#Problem_5 <Breaking the records>.
def breakingRecords(scores):
    x = scores[0] ; y = scores[0] ; maxCount = 0 ; minCount = 0
    for i in scores:

        if i > x :
            maxCount+=1
            x = i
        
        if i < y:
            minCount+=1
            y = i
    return maxCount, minCount


#Problem_6 <Subarray Division>.
n = int(input())
s = list(map(int,input().split()))[:n]
d,m = list(map(int,input().split()))

def birthday(s, d, m):
    count = 0
    for i in range(len(s)-1):
        if (sum(s[i:i+m])) == d:
            count+=1
    if n == 1:
        count = 1
    print(count)


#Problem_7 <Divisible Sum Pairs>.
n , k = list(map(int,input().split()))
ar = list(map(int,input().split()))[:n]
Count = 0
    #ar = list(map(int,input().split()))[:n]
def divisibleSumPairs(n, k, ar):
    for i in range(len(ar)-1):
        h = ar[i]/k
        for j in range(len(ar)-1):
            #if (h+(ar[j+1]/k)) % k  == 0:
            #[1, 3, 2, 6, 1, 2]
            if (ar[i] + (ar[j+1])) % k == 0 and j >= i:
        #if (random.choice(l) + random.choice(l))% k == 0:
                Count +=1
    print(Count)


#Problem_8 <Migratory Birds>.
def migratoryBirds(arr):
    x=list(set(arr))
    y=sorted([(arr.count(i),i) for i in x], key=lambda x:x[0])[::-1]
    for i in range(len(y)):
        if y[0][0]==y[1][0] and y[0][1]>y[1][1]:
            return y[1][1]
        else:
            return y[0][1]

#Problem_9 <Bill Division>.
def bonAppetit(bill, k, b):
    bill.remove(bill[k])
    x = int(b - sum(bill)/2)
    print(x if x>0 else 'Bon Appetit')


#Problem_10 <Day Of Programmer>
def dayOfProgrammer(year):
    # Write your code here
    val, month = 0, 0
    
        
    lst = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    
    if year < 1918 and year % 4 == 0:
        lst[1] += 1
        
        
    if year == 1918:
        lst[1]-=13
    
    if year > 1918 and (year % 400 == 0 or year % 4 == 0 and year % 100 != 0):
            lst[1]+=1
        

        
    x = lambda i: sum(lst[0:i])
    
    for i in range(1, 13):
        if x(i) < 256:
            val += 1
            month = i
      
    day = 256-x(month)

    date = "{}.09.{}".format(day, year)
    
    return date


#Problem_11 <Sales By Match>
def sockMerchant(n, ar):
    # Write your code here
    count = 0;

    set_ar = list(set(ar))
    
    dic = {key:0 for key in set_ar}

    for i in dic:
        for j in ar:
            if i == j:
                dic[i] += 1
                if dic[i] % 2 == 0:
                    count += 1
        
    return count
